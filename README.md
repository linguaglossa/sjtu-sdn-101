# sjtu-sdn-101



## Getting started

In order to do these labs you will need to have the following packages:

- Docker (install `docker.io` for simplicity)
- OpenVSwitch (install `openvswitch-switch` and `openvswitch-common`)
- Kathara (Download it from the [Kathara website](https://www.kathara.org/download.html)

## List of labs

1. SDN basics: controllers, OpenFlow
2. Service function chaining
3. Linux software networking

### Lab 1: SDN

Here we will use the tools to analyze SDN-based network scenarios used in the course of "Internet and Data Centers" at Roma Tre university (Italy). [Source](https://github.com/KatharaFramework/Kathara-Labs/tree/main/main-labs/sdn-openflow).

### Lab 2: Service function chaining

Here we will experiment with the service function chaining used in the course of "New network architectures" at  CNAM (France). [Source](https://cedric.cnam.fr/~seccis/RSX217/) and [Source](https://github.com/abulanov/sfc_app).

### Lab 3: Linux software networking tools

Finally, in our last lab we will experiment with the basics tools which enable software networking programmability, such as namespaces, veth, iptables, container networking. 

## Reports

Before each lab you will receive instructions about the expected deliverable, which may be in one of the following form: 

- Open question to be answered
- Software product/configuration
- Report

---

**A separate `lab-X.md` file will appear before each lab to prepare you for the activity.**

---