# Experimenting with Linux software packet processing

In this lab you will have a hands-on session with Linux tools such as : 

- `namespaces`
- `veth` interfaces
- OvS switch
- traffic control with `tc`

You will find a list of questions, indicated by **QX.Y** as well as a list of tasks, identified by **TW.Z**. You will have to provide a file with the answer to the questions, and the pieces of code resulting from the tasks.


## Intro: creating and connecting namespaces

Let's start by creating two separate namespaces. Remind that a (network) namespace is an "abstraction that makes it appear to the processes within the namespace that they have their own isolated instance of the global resource", in this case the Linux networking stack.

**T0.0** create two namespaces with the following command:

```
sudo ip netns add dom1
sudo ip netns add dom2
```
This will create two separate "domains" that will have a virtually isolated copy of the Linux networking stack.

**Q0.0** Execute the command `ifconfig` or `ip link` from within a namespaces, and compare it with the same command in the host. Repeat with the command `top`. Briefly explain the differences and similarities.
You can execute commands on any namespace with this syntax:

```
sudo ip netns exec <domain> <command>
```

**T0.1** Now let's connect the two namespaces with a veth pair: use the following commands

```
sudo ip link add veth1 type veth peer name veth2
sudo ip link set veth1 netns dom1
sudo ip link set veth2 netns dom2
```

Assign the IP addresses to each veth interface from within their namespaces in order to be able to ping each other. 

**Q0.1** While pinging across namespaces, capture (with tcpdump) some ICMP packets from one of the namespaces.
With the help of this [https://stackoverflow.com/questions/51358018/linux-cooked-capture-in-packets](link), explain the visible header, and why there is a single Ethernet address. Where is it taken from?

## Modeling unstable channels in software

From the manpage, "NetEm is an enhancement of the Linux traffic control facilities that allow to add delay, packet loss, duplication and more other characteristics to packets outgoing from a selected network interface."

It is possible to emulate lossy channels, as well as introducing quality modifiers (rate, duplication, delay, ...).

We will try now to add a 50% loss probability to one of the two veth pairs that we created.

**T1.1** Exec a bash shell on one of the two namespaces, then run the following command to add a custom loss rate : 

```
tc qdisc add dev veth2 root netem loss 50%
```

Verify with a ping that you indeed are experiencing packet loss

```
ping -f -c 50 <IP-OF-OTHER-VETH>
```

**Q1.1** Is the loss that you introduced bidirectional?

## Bridging and switching

Now you will create two different namespaces and connect them through an OvS switch 

```
+------------+   +------------------------+   +------------+
|   Left ns  |   |      OVS Switch        |   |  Right ns  |
|   +----+   |   |    +-------+-------+   |   | +----+     |
|   |Port|   |   |    | S-left|       |   |   | |Port|     |
|   | 1  |---|---|----|       |S-Right|---|---| | 2  |     |
|   +----+   |   |    +-------+-------+   |   | +----+     |
+------------+   +----+-------------------+   +------------+

```

**T1.2** Instantiate an ovs switch (similarly to what we have done in the first lab. 
Remember that you can use the command `sudo ovs-vsctl add-br <SW>` to create a switch named <SW>, and `sudo ovs-vsctl add-port <SW> <PORT>` to add a port named <PORT> to the switch <SW>.
Remember that you need a first veth link between the first namespace and the switch, and another from the ovs switch to the second namespace.
Remember that you will only need IP addresses at the endpoint level (and not within the switch!).

Provide the configuration script that you used.

**T1.2** Connect the ovs-switch to a controller, within the host machine. Use the default OpenFlow learning switch application (take inspiration from the config we have seen in the first lab). Verify that you can ping between the two namespaces.

**Q1.1** Which namespace does the ovs switch belong to? Can you run an OvS switch from within a separate namespace? Briefly justify your findings. 


## Multi-level routing

You will now reproduce a routing application using SDN concepts with the tools that we already explored. 

**T2.1** Create three namespaces, connected linearly with veth pairs (`ns1 <----> ns2 <----> ns3`). Use the `route` command to make the three containers pingable with each other.

**T2.2** While previous routing was manually configured, try to reproduce the example of the [https://github.com/KatharaFramework/Kathara-Labs/tree/main/main-labs/sdn-openflow/pox/09-pox-routing](Kathara Lab number 9) to reproduce a multi-namespace connections of elements managed by some ovs switches.
Do not use for this lab a kathara configuration: try to recreate the namespaces and the veth pairs yourself, and then attach the ovs switches with the routing controller. Submit the configuration scripts that you used to create your topology. 
