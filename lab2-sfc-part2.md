# Experimenting with SFC in a SDN domain

In this part you will explore the capabilities of SDN-based networks to support function chaining

The objective of this lab will be to reproduce a system that implements SFC to work with a Kathara SDN network and a POX controller. 

## Mini-project

You are expected to provide a Kathara configuration that implements SFC (Routing and function execution). 

You can get inspiration from the old project located [here](https://github.com/abulanov/sfc_app).

In practice, you will need to define : 

- A VNF database (SQLITE in the old project)
- A few functions (docker containers, you can use simple nodes in a Kathara lab)
- POX as the SDN controller

In the example project, the controller has also an HTTP server to implement REST requests. This is optional, and is not required. 
