# Experimenting with OpenFlow and Kathara

In this part you will explore the capabilities of SDN-based networks using Kathara, a tool developed by the research group of Roma 3 University (Italy).

You will find a list of questions, indicated by **QX.Y** as well as a list of tasks, identified by **TW.Z**. You will have to provide a file with the answer to the questions, and the pieces of code resulting from the tasks. 


## Intro

As first task, try to install and run Kathara. The network topology is based on a simple `lab.conf` file that describes all the information related to the network connections of machines (hosts, controllers, switches and routers). Such elements are based on *containers*, that are used to emulate a machine that is distinct from your own host. A Linux terminal will be created and a sequence of command specified in the files `machine.startup` will be executed. 
You can also specify the image of such containers (for instance, to implement an OSPF router or an OpenFlow controller).

**T0.0** Clone the repository `https://github.com/KatharaFramework/Kathara-Labs` and go into the path `main-labs/sdn-openflow/pox`. Use Kathara to start the first lab `01-pox-controller` and follow the instruction in the local README file.

In order to launch the lab, you will need to run the command:

`sudo kathara lstart`

from the root of the lab (the one where the file `lab.conf` is present).

Some terminals will be launched, but you can also connect to any of the machines via the command:

`kathara connect <machine_name>`

as in `kathara connect controller`.

To stop the lab, you will just need to launch the command:

`sudo kathara lclean`.

Check that everything works fine, and explore the configuration files present in the root directory. 

## Part 1

In the first lab, you have instantiated a series of network elements, and your topology is now deployed. 

---

**Q1.1** With the help of the POX documentation, find the source code of the learning component of the controller. What is the function that is handling the `packet_in` event? Describe schematically how it works. 

**Q1.2** After you launch the controller you will see a message like `INFO:openflow.of_01:[f6-c0-be-8f-27-4e 1] ...`. Interpret the message: what do the different portions of such log represent? 

---

We will now try to make the topology more complex.

**T1.1** With the help of the Kathara documentation, try to add a new switch in the topology, and connect it to the controller. 

---

## Part 2

With POX you can write your own component. Follow the instructions of the lab `02-pox-core-object` and then answer the following questions.

---

**Q2.1** Where are the custom components A.py and B.py located? Detail the path and the machine where they will be executed.

---

**T2.1** To verify that the components are indeed executed in some particular machine, try to modify the message to be sent by one of those components to include some operating system information (for example, the hostname or the ip address: you can import the python library `os` or similar).

## Part 3

We have seen in class that OpenFlow switches generate events to be sent to the controller. In this part of the lab `03-pox-events` we will see how POX have the visibility of the event. Follow the instructions of the local README file and then answer these questions.

---

**Q3.1** Try to launch several pings between two hosts machines. You can use a `ping -f` to flood packets, or you can simply start, stop and restart a ping. Explain how many `packet_in` events are generated, and why. 

---

**T3.1** Use `tcpdump` to capture the packets received by the controller before and during a ping session. Export the capture in the directory `/shared`.

## Part 4

Controllers can also deal with packet crafting (generate, analyze, duplicate). Although this operation is time consuming (and it would be better suited for dataplane processing) it is worth to explore how it could be used in an SDN network. Follow the instructions of the lab `04-pox-work-with-packets` and then proceed with the following tasks. 

**T4.1** Packet generation is performed by the `scapy` packet processing library. Look at the source code of this packet creation, and then modify the source file to generate several different MAC headers. Then, implement an "Access control List" at the controller level, to allow some headers and forbid some other headers. 

## Part 5

You are now familiar with most of the operations that can be done via a SDN controller. Keep in mind that we are exploring two different SDN approaches : 

- Data plane programmability (You code the SDN controller elements individually and install the desired behavior)
- Network configuration and management (You use an SDN system *as is* and you tweak the parameters according to your use case).

Although the first approach is the most common in research papers and prototype demonstrators, the second approach is the one that is actually more common. In practice, you will use SDN controllers as they are provided to you, with little to no modification to be made on their source code. However, you will extensively interact with the **interfaces** of such SDN controllers via APIs or via a declarative language. 

### Optional part

You can optionally finalize the labs from `05-pox-datapaths` to `09-pox-routing`. 

### Final questions

**Q5.1** Describe how you would build a SDN system with the following device and service features:

**DEVICES**

- 5 host machines
- 4 switches in a hierarchy of two separate layers
- 1 single controller

**SERVICES**

- L2 switching
- L3 routing
- Access control List based on MAC addresses
- Firewall system based on TCP/UDP portions

Describe the components, the basic steps that you would need to take and the main operations in some use cases of your choice. It is not required to provide a Kathara implementation, but if you come up with one, that is even better. 